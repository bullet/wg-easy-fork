ARG HASH=sha256:4c06b641d1bbe1d93c4acc24409cecba32e33c42ccfc49f3bad5189e354cb96d

ARG DIST=node:14.20-alpine3.15

FROM ${DIST}@${HASH} AS build_node_modules

# Copy Web UI
COPY src/ /app/
WORKDIR /app
RUN npm ci --production

# Copy build result to a new image.
# This saves a lot of disk space.
FROM ${DIST}@${HASH}

LABEL maintainer="Secven Security"

COPY --from=build_node_modules /app /app

# Move node_modules one directory up, so during development
# we don't have to mount it in a volume.
# This results in much faster reloading!
#
# Also, some node_modules might be native, and
# the architecture & OS of your development machine might differ
# than what runs inside of docker.
RUN \
    mv /app/node_modules /node_modules && \
    # Enable this to run `npm run serve` #
    npm i -g nodemon

# Update Linux packages 
RUN \
    # Secured to https download #
    sed -i 's/http:/https:/' /etc/apk/repositories && \
    apk --update upgrade --no-cache && \
    # Add packeges is select version #
    apk add --no-cache \
    "dumb-init>=1.2.5-r1" \
    "wireguard-tools>=1.0.20210914-r0" \
    "tzdata>=2022f-r0"
    # ln -s /usr/share/zoneinfo/Universal /etc/localtime 
                    
SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

# Tune network  
RUN \
    echo -e " \n\
    fs.file-max = 51200 \n\
    \n\
    net.core.rmem_max = 67108864 \n\
    net.core.wmem_max = 67108864 \n\
    net.core.netdev_max_backlog = 250000 \n\
    net.core.somaxconn = 4096 \n\
    \n\
    net.ipv4.tcp_syncookies = 1 \n\
    net.ipv4.tcp_tw_reuse = 1 \n\
    net.ipv4.tcp_tw_recycle = 0 \n\
    net.ipv4.tcp_fin_timeout = 30 \n\
    net.ipv4.tcp_keepalive_time = 1200 \n\
    net.ipv4.ip_local_port_range = 10000 65000 \n\
    net.ipv4.tcp_max_syn_backlog = 8192 \n\
    net.ipv4.tcp_max_tw_buckets = 5000 \n\
    net.ipv4.tcp_fastopen = 3 \n\
    net.ipv4.tcp_mem = 25600 51200 102400 \n\
    net.ipv4.tcp_rmem = 4096 87380 67108864 \n\
    net.ipv4.tcp_wmem = 4096 65536 67108864 \n\
    net.ipv4.tcp_mtu_probing = 1 \n\
    net.ipv4.tcp_congestion_control = hybla \n\
    # for low-latency network, use cubic instead \n\
    # net.ipv4.tcp_congestion_control = cubic \n\
    " | sed -e 's/^\s\+//g' | tee -a /etc/sysctl.conf && \
    mkdir -p /etc/security && \
    echo -e " \n\
  * soft nofile 51200 \n\
  * hard nofile 51200 \n\
    " | sed -e 's/^\s\+//g' | tee -a /etc/security/limits.conf  

# Expose Ports
EXPOSE 51820/udp
EXPOSE 51821/tcp

# Set Environment
ENV DEBUG=Server,WireGuard

# Run Web UI
WORKDIR /app

CMD ["/usr/bin/dumb-init", "node", "server.js"]
