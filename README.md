# WireGuard Easy fork for Secure VPN

[![Docker](https://img.shields.io/docker/v/secven/wg-easy/latest)](https://hub.docker.com/r/secven/wg-easy)
[![Docker](https://img.shields.io/docker/pulls/secven/wg-easy.svg)](https://hub.docker.com/r/secven/wg-easy)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/secven/wg-easy/latest)
![YouTube Channel Subscribers](https://img.shields.io/youtube/channel/subscribers/UCb0MdhCVUA-JeGysDTMjmXg?style=social)

<p align="center">
  <img src="./assets/wireguard-logo2.png" width="800" />
</p>

<img align="right" alt="WireGuard" width="150"
src="./assets/-WireGuard-VPN.png" />

<p>WireGuard® - это чрезвычайно простой, но быстрый и современный VPN, использующий самую современную криптографию. Его цель - быть быстрее, проще, компактнее и полезнее, чем IPsec, избегая при этом огромной головной боли. Он должен быть значительно более производительным, чем OpenVPN. WireGuard разработан как VPN общего назначения для работы как на встроенных интерфейсах, так и на суперкомпьютерах, пригодный для различных обстоятельств. Изначально выпущенный для ядра Linux, сейчас он является кроссплатформенным (Windows, macOS, BSD, iOS, Android) и широко развертывается. В настоящее время он находится в стадии активной разработки, но уже сейчас его можно считать самым безопасным, простым в использовании и простым VPN-решением в отрасли.</p>

### Этот форк, был сделан для курса Secure VPN, от Свободный Волк 🐺

* СТАРТ КУРСА: Выбор безопасного хостинга для своего Secure VPN / BulletProof ISP Offshore Hosting https://youtu.be/avv7EQFBSp8

* Сам курс и приватный канал https://t.me/Libre_Wolf/452

## Features

* All-in-one: WireGuard + Web UI.
* Easy installation, simple to use.
* List, create, edit, delete, enable & disable clients.
* Show a client's QR code.
* Download a client's configuration file.
* Statistics for which clients are connected.
* Tx/Rx charts for each connected client.
* DNS Unbound support.
* SSL/HTTPS WEB support.
* Use HTTP basic auth protect page Web UI.

## Requirements

* A host with a kernel that supports WireGuard (all modern kernels).
* A host with Docker installed.
* Recomended use [Linux-Libre](https://www.fsfla.org/ikiwiki/selibre/linux-libre/)

## Installation

### 1. Install Docker

```bash
~$ sudo apt update && sudo apt -y upgrade

# For Devuan
~$ sudo apt -y install docker docker-compose

# For Debian
~$ curl -sSL https://get.docker.com/ | CHANNEL=stable sh

~$ sudo sh -c 'curl -L https://github.com/docker/compose/releases/download/v$(curl -Ls https://www.servercow.de/docker-compose/latest.php)/docker-compose-$(uname -s)-$(uname -m) > /usr/local/bin/docker-compose'

~$ sudo chmod +x /usr/local/bin/docker-compose
```

### 2. Run WireGuard Easy

<pre>
~$ git https://codeberg.org/bullet/wg-easy-fork.git
~$ cd wg-easy-fork
~$ cp .env.example .env && nano .env
~$ sudo docker-compose up -d
</pre>

### 🔧 Default change options `.env`

> 💡 Replace `IP_SERVER` with your WAN IP, or a Dynamic DNS hostname.
> 
> 💡 Replace `WEB_PASSWORD` with a password to log in on the Web UI.
>
> 💡 The Web UI will now be available on `https://127.0.0.1:51821`

### 🛡 Security Protection web UI

> 💡 Replace `AUTH_WEB_BASIC` Auth basic protection default `no` 
>
> 💡 Replace `AUTH_WEB_USER` with a user to log in on the Auth basic
>
> 💡 Replace `AUTH_WEB_PASSWORD` with a password to log in on the Auth basic

### 3. Sponsor

Are you enjoying this project? [Buy me a beer!](https://t.me/ArchiveOpenSource/72) 🍻

## Options for `.env`

These options can be configured by setting environment variables using `KEY="VALUE"` in the `.env`

| Env | Default | Example | Description |
| - | - | - | - |
| `IP_SERVER` | `127.0.0.1` | `vpn.bullet.li` | The public hostname of your VPN server. |
| `WIREGUARD_DNS` | `10.2.0.100` | `93.95.224.28` | DNS server clients will use. Default use unbound. |
| `WIREGUARD_PORT` | `51820` | `53` | The public UDP port of your VPN server. |
| `WIREGUARD_MTU` | `1420` | `1420` | The MTU the clients will use. Server uses default WG MTU. |
| `WEB_PASSWORD` | `changeme` | `foobar123` | When set, requires a password when logging in to the Web UI. |
| `WEB_UI_PORT` | `51821` | `8443` | When set listen port, logging in to the Web UI. |
| `AUTH_WEB_BASIC` | `no` | `yes` | Enforce login before accessing a resource or the whole site using HTTP basic auth method. |
| `AUTH_WEB_USER` | `changeme` | `root` | Username for HTTP basic auth. |
| `AUTH_WEB_PASSWORD` | `changeme` | `foobar123` | Password for HTTP basic auth. |
| `LOGGING_DRIVER` | `none` | `local` | Logging driver options for docker-compose.yml - Recomended use none |


## Options for `docker-compose.yml` section services `wg-easy`

These options can be configured by setting environment variables using `KEY="VALUE"` in the `docker-compose.yml`

| Env | Default | Example | Description |
| - | - | - | - |
| `PASSWORD` | `changeme` | `foobar123` | When set, requires a password when logging in to the Web UI. |
| `WG_HOST` | `192.168.1.233` | `vpn.bullet.li` | The public hostname of your VPN server. |
| `WG_PORT` | `51820` | `5353` | The public UDP port of your VPN server. WireGuard will always listen on `51820` inside the Docker container. |
| `WG_MTU` | `1420` | `1420` | The MTU the clients will use. Server uses default WG MTU. |
| `WG_PERSISTENT_KEEPALIVE` | `0` | `25` | Value in seconds to keep the "connection" open. If this value is 0, then connections won't be kept alive. |
| `WG_DEFAULT_ADDRESS` | `10.8.0.x` | `10.6.0.x` | Clients IP address range. |
| `WG_DEFAULT_DNS` | `10.2.0.100` | `93.95.224.28, 95.215.19.53` | DNS server clients will use. |
| `WG_ALLOWED_IPS` | `0.0.0.0/0, ::/0` | `192.168.15.0/24, 10.0.1.0/24` | Allowed IPs clients will use. |
| `WG_PRE_UP` | `...` | - | See [config.js](https://git.disroot.org/bullet/wg-easy-fork/src/branch/main/src/config.js#L19) for the default value. |
| `WG_POST_UP` | `...` | `iptables ...` | See [config.js](https://git.disroot.org/bullet/wg-easy-fork/src/branch/main/src/config.js#L20) for the default value. |
| `WG_PRE_DOWN` | `...` | - | See [config.js](https://git.disroot.org/bullet/wg-easy-fork/src/branch/main/src/config.js#L27) for the default value. |
| `WG_POST_DOWN` | `...` | `iptables ...` | See [config.js](https://git.disroot.org/bullet/wg-easy-fork/src/branch/main/src/config.js#L28) for the default value. |

> If you change `WIREGUARD_PORT`, make sure to also change the exposed port.

## Updating

To update to the latest version, simply run:

```bash
~$ sudo docker-compose down
~$ sudo docker-compose pull
~$ sudo docker-compose up -d
```

## Common Use Cases

* [Using WireGuard-Easy with Pi-Hole](soon)
* [Using WireGuard-Easy with bunkerweb](soon)

<hr>

## TODO

* TLS over TOR
* add more security

## Author

> #### Project creator: https://github.com/WeeJeWel/wg-easy
>
> #### Donate to: https://github.com/sponsors/WeeJeWel

### License

`Distributed under the GPL V3 License. See LICENSE for more information`

<hr>