'use strict'

new Vue({
	el: '#app',
	data: {
		authenticated: null,
		authenticating: false,
		password: null,
		requiresPassword: null,

		clients: null,
		clientsPersist: {},
		clientDelete: null,
		clientCreate: null,
		clientCreateName: '',
		clientEditName: null,
		clientEditNameId: null,
		clientEditAddress: null,
		clientEditAddressId: null,
		qrcode: null,

		currentRelease: null,
		latestRelease: null,

		chartOptions: chartDataParams
	},
	methods: {
		dateTime: value => {
			return new Intl.DateTimeFormat(undefined, {
				year: 'numeric',
				month: 'short',
				day: 'numeric',
				hour: 'numeric',
				minute: 'numeric'
			}).format(value)
		},

		async refresh({ updateCharts = false } = {}) {
			if (!this.authenticated) return

			const clients = await this.api.getClients()
			this.clients = clients.map(client => {
				if (!this.clientsPersist[client.id]) {
					this.clientsPersist[client.id] = {}
					this.clientsPersist[client.id].transferRxHistory = Array(50).fill(0)
					this.clientsPersist[client.id].transferRxPrevious = client.transferRx
					this.clientsPersist[client.id].transferTxHistory = Array(50).fill(0)
					this.clientsPersist[client.id].transferTxPrevious = client.transferTx
				}

				// Debug
				/*
				client.transferRx =
					this.clientsPersist[client.id].transferRxPrevious +
					Math.random() * 1000
				client.transferTx =
					this.clientsPersist[client.id].transferTxPrevious +
					Math.random() * 1000
					*/

				if (updateCharts) {
					this.clientsPersist[client.id].transferRxCurrent =
						client.transferRx -
						this.clientsPersist[client.id].transferRxPrevious
					this.clientsPersist[client.id].transferRxPrevious = client.transferRx
					this.clientsPersist[client.id].transferTxCurrent =
						client.transferTx -
						this.clientsPersist[client.id].transferTxPrevious
					this.clientsPersist[client.id].transferTxPrevious = client.transferTx

					this.clientsPersist[client.id].transferRxHistory.push(
						this.clientsPersist[client.id].transferRxCurrent
					)
					this.clientsPersist[client.id].transferRxHistory.shift()

					this.clientsPersist[client.id].transferTxHistory.push(
						this.clientsPersist[client.id].transferTxCurrent
					)
					this.clientsPersist[client.id].transferTxHistory.shift()
				}

				client.transferTxCurrent =
					this.clientsPersist[client.id].transferTxCurrent
				client.transferRxCurrent =
					this.clientsPersist[client.id].transferRxCurrent

				client.transferTxHistory =
					this.clientsPersist[client.id].transferTxHistory
				client.transferRxHistory =
					this.clientsPersist[client.id].transferRxHistory
				client.transferMax = Math.max(
					...client.transferTxHistory,
					...client.transferRxHistory
				)

				client.hoverTx = this.clientsPersist[client.id].hoverTx
				client.hoverRx = this.clientsPersist[client.id].hoverRx

				return client
			})
		},

		login(e) {
			e.preventDefault()

			if (!this.password) return

			if (this.authenticating) return

			this.authenticating = true
			this.api
				.createSession({
					password: this.password
				})
				.then(async () => {
					const session = await this.api.getSession()
					this.authenticated = session.authenticated
					this.requiresPassword = session.requiresPassword
					return this.refresh()
				})
				.catch(err => {
					alert(err.message || err.toString())
				})
				.finally(() => {
					this.authenticating = false
					this.password = null
				})
		},

		logout(e) {
			e.preventDefault()

			this.api
				.deleteSession()
				.then(() => {
					this.authenticated = false
					this.clients = null
				})
				.catch(err => {
					alert(err.message || err.toString())
				})
		},

		createClient() {
			const name = this.clientCreateName
			if (!name) return

			this.api
				.createClient({ name })
				.catch(err => alert(err.message || err.toString()))
				.finally(() => this.refresh().catch(console.error))
		},

		deleteClient(client) {
			this.api
				.deleteClient({ clientId: client.id })
				.catch(err => alert(err.message || err.toString()))
				.finally(() => this.refresh().catch(console.error))
		},

		enableClient(client) {
			this.api
				.enableClient({ clientId: client.id })
				.catch(err => alert(err.message || err.toString()))
				.finally(() => this.refresh().catch(console.error))
		},

		disableClient(client) {
			this.api
				.disableClient({ clientId: client.id })
				.catch(err => alert(err.message || err.toString()))
				.finally(() => this.refresh().catch(console.error))
		},

		updateClientName(client, name) {
			this.api
				.updateClientName({ clientId: client.id, name })
				.catch(err => alert(err.message || err.toString()))
				.finally(() => this.refresh().catch(console.error))
		},

		updateClientAddress(client, address) {
			this.api
				.updateClientAddress({ clientId: client.id, address })
				.catch(err => alert(err.message || err.toString()))
				.finally(() => this.refresh().catch(console.error))
		}
	},

	filters: {
		bytes,
		timeago: value => {
			return timeago().format(value)
		}
	},

	mounted() {
		this.api = new API()
		this.api
			.getSession()
			.then(session => {
				this.authenticated = session.authenticated
				this.requiresPassword = session.requiresPassword
				this.refresh({
					updateCharts: true
				}).catch(err => {
					alert(err.message || err.toString())
				})
			})
			.catch(err => {
				alert(err.message || err.toString())
			})

		setInterval(() => {
			this.refresh({
				updateCharts: true
			}).catch(console.error)
		}, 2e3)

		Promise.resolve()
			.then(async () => {
				const currentRelease = await this.api.getRelease()
				const latestRelease = await fetch(
					'https://git.disroot.org/bullet/wg-easy-fork/raw/branch/main/docs/changelog.json'
				)
					.then(res => res.json())
					.then(releases => {
						const releasesArray = Object.entries(releases).map(
							([version, changelog]) => ({
								version: parseInt(version, 10),
								changelog
							})
						)
						releasesArray.sort((a, b) => {
							return b.version - a.version
						})

						return releasesArray[0]
					})
					.catch(console.error)

				console.log(`Current Release: ${currentRelease}`)
				console.log(`Latest Release: ${latestRelease.version}`)

				if (currentRelease >= latestRelease.version) return

				this.currentRelease = currentRelease
				this.latestRelease = latestRelease
			})
			.catch(console.error)
	}
})
